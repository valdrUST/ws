from flask import Flask, escape, request, Blueprint, jsonify, make_response
from . import Graficador
from os import mkdir, path
import base64
import json

server_report = Blueprint('app', __name__)
class Server():
    def __init__(self, name, *args, **kwargs):
        self.app = Flask(name)
        self.app.register_blueprint(server_report)

@server_report.route('/ws/ws-reportes',methods=['GET'])
def index():
    return "<h1>WS-REPORTES</h1>"

@server_report.route('/ws/ws-reportes/obtener_graficas_promedios',methods=['POST'])
def obtener_graficas_promedios():
    respuesta = {
        "estado": 0,
        "datos": {}
    }
    try:
        datos = request.get_json()
        json_graficas = {
            "grafica_grupo":"",
            "grafica_departamento":"",
            "grafica_division":"",
            "grafica_facultad":""
        }
        grafica_datos_promedio_grupo = {
            "title":"",
            "data":[],
            "labels":[],
            "xticks":[],
            "xlabel":"Semestre\n(Figura A)",
            "ylabel":""
        }
        grafica_datos_promedio_grupo["title"] = datos["promedioGruposAP"][0]["nombre"]
        grafica_datos_promedio_grupo["labels"].append("Promedio Calificacion Alumno")
        porcentajes = []
        for dato in datos["promedioGruposAP"]:
            grafica_datos_promedio_grupo["xticks"].append(dato["id_semestre"])
            porcentajes.append(float(dato["valor"]))
        grafica_datos_promedio_grupo["data"].append(porcentajes)
        
        grafica_datos_promedio_grupo["labels"].append("Promedio Calificacion Profesor")
        porcentajes = []
        for dato in datos["promedioGruposPP"]:
            porcentajes.append(float(dato["valor"]))
        grafica_datos_promedio_grupo["data"].append(porcentajes)

        grafica_datos_promedio_grupo["labels"].append("Calificacion Alumno")
        porcentajes_prof = []
        for xtick in grafica_datos_promedio_grupo["xticks"]:
            if(xtick == datos["promedioProfesorAP"][0]["id_semestre"]):
                porcentajes_prof.append(float(datos["promedioProfesorAP"][0]["valor"]))
            else:
                porcentajes_prof.append(None)
        grafica_datos_promedio_grupo["data"].append(porcentajes_prof)
    
        grafica_datos_promedio_grupo["labels"].append("Calificacion Profesor")
        porcentajes_prof = []
        for xtick in grafica_datos_promedio_grupo["xticks"]:
            if(xtick == datos["promedioProfesorPP"][0]["id_semestre"]):
                porcentajes_prof.append(float(datos["promedioProfesorPP"][0]["valor"]))
            else:
                porcentajes_prof.append(None)
        grafica_datos_promedio_grupo["data"].append(porcentajes_prof)
        
        datos["promedioGruposPP"]
        grafica_datos_promedio_departamento = {
            "title":"",
            "data":[],
            "labels":[],
            "xticks":[],
            "xlabel":"Semestre\n(Figura B)",
            "ylabel":""
        }
        grafica_datos_promedio_departamento["title"] = datos["promedioDepartamentoAP"][0]["nombre"]

        grafica_datos_promedio_departamento["labels"].append("Promedio Calificacion Alumno")
        porcentajes = []
        for dato in datos["promedioDepartamentoAP"]:
            grafica_datos_promedio_departamento["xticks"].append(dato["id_semestre"])
            porcentajes.append(float(dato["valor"]))
        grafica_datos_promedio_departamento["data"].append(porcentajes)

        grafica_datos_promedio_departamento["labels"].append("Promedio Calificacion Profesor")
        porcentajes = []
        for dato in datos["promedioDepartamentoPP"]:
            porcentajes.append(float(dato["valor"]))
        grafica_datos_promedio_departamento["data"].append(porcentajes)
        grafica_datos_promedio_departamento["labels"].append("Calificacion Alumno")
        porcentajes_prof = []
        for xtick in grafica_datos_promedio_departamento["xticks"]:
            if(xtick == datos["promedioProfesorAP"][0]["id_semestre"]):
                porcentajes_prof.append(float(datos["promedioProfesorAP"][0]["valor"]))
            else:
                porcentajes_prof.append(None)
        grafica_datos_promedio_departamento["data"].append(porcentajes_prof)
        grafica_datos_promedio_departamento["labels"].append("Calificacion Profesor")
        porcentajes_prof = []
        for xtick in grafica_datos_promedio_departamento["xticks"]:
            if(xtick == datos["promedioProfesorPP"][0]["id_semestre"]):
                porcentajes_prof.append(float(datos["promedioProfesorPP"][0]["valor"]))
            else:
                porcentajes_prof.append(None)
        grafica_datos_promedio_departamento["data"].append(porcentajes_prof)
        
        datos["promedioDepartamentoPP"]
        grafica_datos_promedio_division = {
            "title":"",
            "data":[],
            "labels":[],
            "xticks":[],
            "xlabel":"Semestre\n(Figura C)",
            "ylabel":""
        }
        grafica_datos_promedio_division["title"] = datos["promedioDivisionAP"][0]["nombre"]
        grafica_datos_promedio_division["labels"].append("Promedio Calificacion Alumno")
        porcentajes = []
        for dato in datos["promedioDivisionAP"]:
            grafica_datos_promedio_division["xticks"].append(dato["id_semestre"])
            porcentajes.append(float(dato["valor"]))
        grafica_datos_promedio_division["data"].append(porcentajes)
        grafica_datos_promedio_division["labels"].append("Promedio Calificacion Profesor")
        porcentajes = []
        for dato in datos["promedioDivisionPP"]:
            porcentajes.append(float(dato["valor"]))
        grafica_datos_promedio_division["data"].append(porcentajes)
        grafica_datos_promedio_division["labels"].append("Calificacion Alumno")
        porcentajes_prof = []
        for xtick in grafica_datos_promedio_division["xticks"]:
            if(xtick == datos["promedioProfesorAP"][0]["id_semestre"]):
                porcentajes_prof.append(float(datos["promedioProfesorAP"][0]["valor"]))
            else:
                porcentajes_prof.append(None)
        grafica_datos_promedio_division["data"].append(porcentajes_prof)
        grafica_datos_promedio_division["labels"].append("Calificacion Profesor")
        porcentajes_prof = []
        for xtick in grafica_datos_promedio_division["xticks"]:
            if(xtick == datos["promedioProfesorPP"][0]["id_semestre"]):
                porcentajes_prof.append(float(datos["promedioProfesorPP"][0]["valor"]))
            else:
                porcentajes_prof.append(None)
        grafica_datos_promedio_division["data"].append(porcentajes_prof)
        
        datos["promedioDivisionPP"]
        grafica_datos_promedio_facultad = {
            "title":"",
            "data":[],
            "labels":[],
            "xticks":[],
            "xlabel":"Semestre\n(Figura D)",
            "ylabel":""
        }
        grafica_datos_promedio_facultad["title"] = "FACULTAD"
        grafica_datos_promedio_facultad["labels"].append("Promedio Calificacion Alumno")
        porcentajes = []
        for dato in datos["promedioFacultadAP"]:
            grafica_datos_promedio_facultad["xticks"].append(dato["id_semestre"])
            porcentajes.append(float(dato["valor"]))
        grafica_datos_promedio_facultad["data"].append(porcentajes)
        grafica_datos_promedio_facultad["labels"].append("Promedio Calificacion Profesor")
        porcentajes = []
        for dato in datos["promedioFacultadPP"]:
            porcentajes.append(float(dato["valor"]))
        grafica_datos_promedio_facultad["data"].append(porcentajes)
        grafica_datos_promedio_facultad["labels"].append("Calificacion Alumno")
        porcentajes_prof = []
        for xtick in grafica_datos_promedio_facultad["xticks"]:
            if(xtick == datos["promedioProfesorAP"][0]["id_semestre"]):
                porcentajes_prof.append(float(datos["promedioProfesorAP"][0]["valor"]))
            else:
                porcentajes_prof.append(None)
        grafica_datos_promedio_facultad["data"].append(porcentajes_prof)
        grafica_datos_promedio_facultad["labels"].append("Calificacion Profesor")
        porcentajes_prof = []
        for xtick in grafica_datos_promedio_facultad["xticks"]:
            if(xtick == datos["promedioProfesorPP"][0]["id_semestre"]):
                porcentajes_prof.append(float(datos["promedioProfesorPP"][0]["valor"]))
            else:
                porcentajes_prof.append(None)
        grafica_datos_promedio_facultad["data"].append(porcentajes_prof)
    except:
        respuesta["estado"] = 500  
        respuesta["mensaje"] = "Error en el formato de datos"
        return json.dumps(respuesta)
    try:
        json_graficas["grafica_grupo"] = Graficador(grafica_datos_promedio_grupo).crear_grafica_lineal()
        json_graficas["grafica_departamento"] = Graficador(grafica_datos_promedio_departamento).crear_grafica_lineal()
        json_graficas["grafica_division"] = Graficador(grafica_datos_promedio_division).crear_grafica_lineal()
        json_graficas["grafica_facultad"] = Graficador(grafica_datos_promedio_facultad).crear_grafica_lineal()
        respuesta["estado"] = 200
        respuesta["datos"] = json_graficas
    except:
        respuesta["estado"] = 500  
        respuesta["mensaje"] = "Error en la creacion de las graficas"
    return json.dumps(respuesta)
