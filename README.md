# WS-Reportes

## Dependencias
Ejecutar el script dependencias.sh para preparar el servidor
```
./dependencias.sh
```

## Ejecución
Ejecutar el script ejecutar.sh para levantar el servidor de pruebas
```
./ejecutar.sh
```
