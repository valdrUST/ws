#!/bin/python3

from main import application
from waitress import serve
serve(application, unix_socket='/run/waitress/ws-reportes.sock',unix_socket_perms='666',ident="ws-reportes",threads=20)
