#!/bin/sh
# Crea el directorio WS
rutaDefaultWS="/home/prdcn_deploy/ws"
if [-d "$rutaDefaultWS" ]; then
        cd $rutaDefaultWS
else
        mkdir $rutaDefaultWS
        cd $rutaDefaultWS
fi

# Directorio que contiene la aplicación
directorioSitio="ws_reportes"
# Descarga o actualiza el código de acuerdo al estatus
if [ -d "$directorioSitio" ]; then
        # La aplicación ya existe, sólo actualiza
        cd $directorioSitio
        git checkout -- .
        git reset HEAD .
        git clean -df
        git checkout -- .
        git pull origin pruebas
else
        # La aplicación no existe, descarga por completo
        git clone -b master https://JavGarC@bitbucket.org/ctlahuelp/ws-reportes.git $directorioSitio
        cd $directorioSitio
        sh dependencias.sh
        sh crearSitio.sh
fi
